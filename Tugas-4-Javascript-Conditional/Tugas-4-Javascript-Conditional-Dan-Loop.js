//Soal 1
var nilai = 25;
if ( nilai >= 85 ) {
    console.log("indeksnya A")
} else if(nilai >= 75 && nilai < 85) {
    console.log("indeksnya B")
} else if (nilai >=65 && nilai < 75){
    console.log("indeksnya C")
}
else if (nilai >=55 && nilai < 65){
    console.log("indeksnya D")
}
else if (nilai < 55){
    console.log("indeksnya E")
}
//Soal 2
var tanggal =16;
var bulan = 8;
var tahun = 1999;
switch(bulan) {
    case 1:   { console.log(tanggal + ' Januari '+tahun); break;}
    case 2:   { console.log(tanggal + ' Februari '+tahun);break;}
    case 3:   { console.log(tanggal + ' Maret '+tahun);break;}
    case 4:   { console.log(tanggal + ' April '+tahun);break;}
    case 5:   { console.log(tanggal + ' Mei '+tahun);break;}
    case 6:   { console.log(tanggal + ' Juni '+tahun);break;}
    case 7:   { console.log(tanggal + ' Juli'+tahun);break;}
    case 8:   { console.log(tanggal + ' Agustus '+tahun);break;}
    case 9:   { console.log(tanggal + ' September '+tahun);break;}
    case 10:   { console.log(tanggal + ' Oktober '+tahun);break;}
    case 11:   { console.log(tanggal + ' November '+tahun);break;}
    case 12:   { console.log(tanggal + ' Desember '+tahun);break;}
    default:  { console.log('Tidak terjadi apa-apa'); }}
//Soal 3
var rows = 3;
var output=""
for(var i=1;i<=rows;i++){
    for(var j=1;j<=i;j++){
        output+="*"
        
    }
    console.log(output);
    output = "";
}

//Soal 4
var m = 18;
var jumlah = 1;
while(m >= 1) { // Loop akan terus berjalan selama nilai deret masih di atas 0
    if(jumlah%2==1){
        if(jumlah%3==0){
            console.log(jumlah+(" - I Love VueJS"))
            console.log("===")
        }else{
console.log(jumlah+(" - I Love Programming"))
        }
    }else{
        if(jumlah%3==0){
            console.log(jumlah+(" - I Love VueJS"))
            console.log("===")
        }else{
        console.log(jumlah+(" - I Love Javascript"))
        }
    }
  jumlah ++; // Menambahkan nilai variable jumlah dengan angka deret
  m--; // Mengubah nilai deret dengan mengurangi 1
  
}