// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = " belajar javascript itu keren";
var ganti_pertama = pertama.substr(0, 4);
var ganti_pertama2 = pertama.substr(11, 7);
var ganti_1 = ganti_pertama.concat(ganti_pertama2);
var ganti_2 = kedua.substr(0,19);
var selesai = ganti_1.concat(ganti_2);
console.log(selesai);

//Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var hasil;
var num1 = Number(kataPertama);
var num2 = Number(kataKedua);
var num3 = Number(kataKetiga);
var num4 = Number(kataKeempat);
hasil = ((num1*num3)+(num2+num4))/num2;
console.log(hasil);

//Soal 3
var kalimat = 'wah javascript itu keren sekali'; 
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);