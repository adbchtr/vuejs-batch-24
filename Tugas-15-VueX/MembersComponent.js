export const MembersComponent = {
    template: `
    <table border=1>
    <tr v-for="member in $store.state.members">
        <td>
            <img width=100 :src="member.photo_profile ? 'http://demo-api-vue.sanbercloud.com' +  member.photo_profile  : 'https://dummyimage.com/16:9x1080'" alt="">
        </td>
        <td>
            <b>Name : </b>{{member.name}}
            <br>
            <b>Address : </b> {{member.address}}
            <br>
            <b>No.HP : </b> {{member.no_hp}}
            <br>
        </td>
        <td>
            <button @click="$store.commit('editmember', member)">Edit</button>
            <button @click="$store.commit('removemember', member.id)">Hapus</button>
            <button @click="$store.commit('uploadphoto', member)">Upload Foto</button>
        </td>
    </tr>
</table>
    `,
    methods : {
        toggle(aksi){
            console.log(aksi)
            this.store.commit('removemember')
        }
    }
        
}